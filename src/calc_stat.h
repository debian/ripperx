#include <ctime>

#include "common.h"
#include "main_data.h"

/* calculates statiscal info to report to the user */
// current is length_processes now

class CalcStat 
{
 public:
  void start_session(const MainData *main_data, _stat *stat);
  void start(const MainData *main_data, _stat *stat,int cur_track, CommonEncoderType cur_type);
  void update(const MainData *main_data, _stat *stat, unsigned int current, int cur_track, CommonEncoderType cur_type);
  void pause();
  void cont();
  void stop(const MainData *main_data, _stat *stat, CommonEncoderType cur_type);
  void stop_session(_stat *stat);
  
 CalcStat() : wav_file_path(0), enc_file_path(0)
    {
      this->init();
    }
  
 private:
  enum CalcStatConst 
  {
    COUNT_BEFORE_GET_AVG = 10
  };
  
  unsigned int total_wav_length, total_mp3_length;
  unsigned int total_wav_length_remain, saved_total_wav_length_remain;
  unsigned int total_mp3_length_remain, saved_total_mp3_length_remain;
  unsigned int length_first_track;
  unsigned int first_track;
  unsigned int first_track_time_remain;
  unsigned int length_last_track;
  unsigned int last_track;
  unsigned int last_track_time_remain;
  float wav_ratio;
  float mp3_ratio;
  time_t session_start_time;
  time_t wav_start_time;
  time_t mp3_start_time;
  time_t pause_time;
  float wav_temp_ratio;
  float mp3_temp_ratio;
  unsigned wav_prev_length_processed;
  unsigned mp3_prev_length_processed;
  int count;
  char *wav_file_path, *enc_file_path;

  void init();
  

};

